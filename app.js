var express = require('express');
var exphbs = require('express-handlebars');
var session = require('express-session');
var passport = require('passport');
var flash    = require('connect-flash');
var LocalStrategy = require('passport-local');
var path = require('path');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var funct = require('./functions.js');

var app = express();

var configDB = require('./config/database.js');
mongoose.connect(configDB.url);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret: 'supernova', saveUninitialized: true, resave: true}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

require('./routes/routes.js')(app, passport);
require('./config/passport')(passport);

var ipaddress = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 3000;
app.listen(port, ipaddress, function () {
    console.log('Example app listening on ' + ipaddress + ':' + port);
});
