var mongoose = require('mongoose');
var bcrypt   = require('bcryptjs');

var userSchema = mongoose.Schema({

    local            : {
        email        : String,
        password     : String,
        isAdmin      : Boolean,
    },
    facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String,
        isAdmin      : Boolean,
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String,
        isAdmin      : Boolean,
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String,
        isAdmin      : Boolean,
    }

});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);
