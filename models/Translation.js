var mongoose = require('mongoose');
var ObjectID = require('mongodb').ObjectID;

// Create a schema
var TranslationSchema = new mongoose.Schema({
  English                  : String,
  Term_Example_English     : String,
  Romanian                 : String,
  Term_Example_Romanian    : String,
  validated                : { type: Boolean, default: false },
  created_at               : { type: Date, default: Date.now },
  updated_at               : { type: Date, default: Date.now },
  created_by               : { type: String },
  updated_by               : { type: String },
  edits                    : [{
    English                : String,
    Term_Example_English   : String,
    Romanian               : String,
    Term_Example_Romanian  : String,
    validated              : { type: Boolean, default: false },
    created_at             : { type: Date, default: Date.now },
    created_by             : { type: String },
  }],
}, { usePushEach: true });

module.exports = mongoose.model('Translation', TranslationSchema);
