angular.module("app", ['ngRoute', 'ngResource'])

.factory('Users', ['$resource', function($resource){
              return $resource('/usersapi/:id', null, {
                'update': { method:'PUT' },
              });
            }])

.controller('AccountsController', ['$scope','$http', 'Users', function ($scope, $http, Users) {

  $scope.users = Users.query();

}]);
