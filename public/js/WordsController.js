
angular.module("app", ['ngRoute', 'ngResource'])

.factory('Translations', ['$resource', function($resource){
              return $resource('/wordsapi/:id', null, {
                'update': { method:'PUT' },
                query: { url:'/wordsapi/search/:searchTerm', method:'GET', isArray: true },
                getnotvalidated: { url:'/wordsapi/getnotvalidated', method:'GET', isArray: true },
                validate: { url:'/wordsapi/validate/:id', method:'GET' },
                validate_edit: { url:'/wordsapi/validate_edit/:id/:editId', method:'GET' },
                update_pending_edit: { url:'/wordsapi/update_pending_edit/:editId', method:'PUT' },
                remove_edit: { url:'/wordsapi/remove_edit/:id/:editId', method:'GET' },
              });
            }])

.controller('TranslationController', ['$scope','$http', 'Translations', function ($scope, $http, Translations) {
  $scope.editing = [];
  $scope.editingPendingEdit = [];
  $scope.translations = [];

  $scope.dosearch = function(){
    $scope.translations = Translations.query({searchTerm : $scope.searchTerm});
  };

  $scope.getnotvalidated = function(){
    $scope.notvalidatedtranslations = Translations.getnotvalidated();
  };

  $scope.validateEdit = function(translationId, eId){
    Translations.validate_edit({id: translationId, editId:eId}).$promise.then(
        function( value ){
          alert("Validat!");
          location.reload();
        },
        function( error ){
          alert("Eroare:" + error);
        }
      );
  };

  $scope.deleteEdit = function(translationId, eId){
    if (!confirm("Sigur doriti sa stergeti?")) { return;}

    Translations.remove_edit({id: translationId, editId:eId}).$promise.then(
        function( value ){
          location.reload();
          alert("Editarea a fost stearsa!");
        },
        function( error ){
          alert("Eroare:" + error);
        }
      );
  };

  $scope.validate = function(index){
    var translation = $scope.notvalidatedtranslations[index];
    Translations.validate({id: translation._id}, translation).$promise.then(
        function( value ){
          $scope.notvalidatedtranslations.splice(index, 1);
          alert("Validat!");
        },
        function( error ){
          alert("Eroare:" + error);
        }
      );
  };

  $scope.delete = function(index){
    if (!confirm("Sigur doriti sa stergeti?")) { return;}

    var translation = $scope.notvalidatedtranslations[index];
    Translations.delete({id: translation._id}, translation).$promise.then(
        function( value ){
          $scope.notvalidatedtranslations.splice(index, 1);
          alert("Sters!");
        },
        function( error ){
          alert("Eroare:" + error);
        }
      );
  };

  $scope.save = function(){
    if(!$scope.newTranslation || $scope.newTranslation.length < 1) return;

    var translation = new Translations(
      {
        English: $scope.newTranslation.English,
        Term_Example_English: $scope.newTranslation.Term_Example_English,
        Romanian: $scope.newTranslation.Romanian,
        Term_Example_Romanian: $scope.newTranslation.Term_Example_Romanian,
        completed: false
      });
    translation.$save(function(translation){
      alert("Adaugat!");
      $scope.newTranslation.English = '';
      $scope.newTranslation.Term_Example_English = '';
      $scope.newTranslation.Romanian = '';
      $scope.newTranslation.Term_Example_Romanian = '';
      $scope.translations.push(translation);
    });
  }

  $scope.update = function(index){
    var translation = $scope.translations[index];
    Translations.update({id: translation._id}, translation);
    $scope.editing[index] = false;
    alert("Editarea a fost trimisa pentru validare!");
  };

  $scope.updatePendingEdit = function(edit){
    Translations.update_pending_edit({editId:edit._id}, edit).$promise.then(
        function( value ){
          $scope.editingPendingEdit[edit._id] = false;
        },
        function( error ){
          alert("Eroare:" + error);
        }
      );
  };

  $scope.edit = function(index){
    $scope.editing[index] = angular.copy($scope.translations[index]);
  };

  $scope.cancel = function(index){
    var original = angular.copy($scope.editing[index]);
    $scope.translations[index] = original;
    $scope.editing[index] = false;
    return original;
  };

  $scope.editPendingEdit = function(edit){
    $scope.editingPendingEdit[edit._id] = angular.copy(edit);
  };

  $scope.cancelPendingEdit = function(edit){
    edit = angular.copy($scope.editingPendingEdit[edit._id]);
    $scope.editingPendingEdit[edit._id] = false;
    return edit;
  };

}])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/', {
          templateUrl: '/words.html',
          controller: 'TranslationController'
      })
      .when('/validation', {
          templateUrl: '/validation.html',
          controller: 'TranslationController'
      });
}]);
