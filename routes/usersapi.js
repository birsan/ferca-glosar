var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();

var mongoose = require('mongoose');
var User = require('../models/user.js');

var morgan = require('morgan')

router.get('/', function(req, res, next) {
  User.find(function (err, todos) {
    if (err) return next(err);

    res.json(todos);
  });
});

module.exports = router;
