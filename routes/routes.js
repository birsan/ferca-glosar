var express = require('express');
var path = require('path');
var router = express.Router();

var wordsapi = require('./wordsapi');
var usersapi = require('./usersapi');

module.exports = function(app, passport) {
    app.use('/wordsapi', wordsapi);
    app.use('/usersapi', usersapi);

    app.get('/', function(req, res, next) {
      res.render('index', { title: 'Express' });
    });

    app.get('/accounts', isLoggedIn, function(req, res, next) {
      res.render('accounts', { title: 'Creare conturi', user: req.user, expressFlash: req.flash('signupMessage') });
    });

    /*app.get('/accounts/:message', isLoggedIn, function(req, res, next) {
      res.render('accounts', { title: 'Creare conturi', user: req.user, expressFlash: "success" });
    });*/

    //app.get('/words/Z3AuClD98cNu1tz', isLoggedIn, function(req, res, next) {
    /*app.get('/words/:id', function(req, res, next) {
        // TODO: use database to store dynamic ids. This way the access can be controlled by removing not wanted ids. For now go hardcoded
        if (req.params.id == 'Z3AuClD98cNu1tz')
          res.render('words', { title: 'Express', user: req.user });
        else
          res.render('index', { title: 'Express' });
    });*/

    app.get('/words', isLoggedIn, function(req, res, next) {
          res.render('words', { title: 'Express', user: req.user });
    });

    app.post('/login', passport.authenticate('local-login', {
            successRedirect : '/words', // redirect to the secure profile section
            failureRedirect : '/', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/', // redirect to the secure profile section
        failureRedirect : '/accounts', // redirect back to the signup page if there is an error
        failureFlash : true, // allow flash messages
        successFlash : "success",
    }));

    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });
};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}
