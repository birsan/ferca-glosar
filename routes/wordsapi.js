var express = require('express');
var bodyParser = require('body-parser');
var mail = require('../config/mail.js');
var router = express.Router();

var mongoose = require('mongoose');
var Translation = require('../models/Translation.js');

var morgan = require('morgan')

//console.log(edit);

router.get('/', function(req, res, next) {
  Translation.find(function (err, todos) {
    if (err) return next(err);

    res.json(todos);
  });
});

router.get('/getnotvalidated', function(req, res, next) {
  Translation.find(
    {$or:[
      {"validated": false},
      {$or:[{"edits.validated": false}]}
    ]},
    function (err, post) {
      if (err) return next(err);

      res.json(post);
  });
});

router.get('/validate/:id', function(req, res, next) {
  Translation.findById(req.params.id, function(err, translation){
    if (err) return next(err);

    translation.validated = true;
    translation.save(function (err) {
        if (err) return next(err);
      });

      res.json(translation);
  });
});

router.get('/validate_edit/:id/:editId', function(req, res, next) {
  Translation.findById(req.params.id, function(err, translation){
    if (err) return next(err);

    translation.edits.forEach(function(edit, index){
      if (edit._id == req.params.editId){
        edit.validated                    = true;
        translation.updated_by            = edit.created_by;
        translation.updated_at            = Date.now();
        translation.English               = edit.English;
        translation.Term_Example_English  = edit.Term_Example_English;
        translation.Romanian              = edit.Romanian;
        translation.Term_Example_Romanian = edit.Term_Example_Romanian;
        translation.edits.splice(index, 1);
      }
    });

    translation.save(function (err) {
        if (err) return next(err);
      });

    res.json(translation);
  });
});

router.get('/remove_edit/:id/:editId', function(req, res, next) {
  Translation.findById(req.params.id, function(err, translation){
    if (err) return next(err);

    translation.edits.forEach(function(edit, index){
      if (edit._id == req.params.editId){
        translation.edits.splice(index, 1);
      }
    });

    translation.save(function (err) {
        if (err) return next(err);
      });

    res.json(translation);
  });
});

router.get('/:id', function(req, res, next) {
  Translation.findById(req.params.id, function (err, post) {
    if (err) return next(err);

    res.json(post);
  });
});

router.get('/search/:searchTerm', function(req, res, next) {
  Translation.find(
  {$and:[
    {"validated": true},
    {$or:[
      {"English": { "$regex": req.params.searchTerm, "$options": "i"}} ,
      {"Term_Example_English": { "$regex": req.params.searchTerm, "$options": "i"}} ,
      {"Romanian": { "$regex": req.params.searchTerm, "$options": "i" }} ,
      {"Term_Example_Romanian": { "$regex": req.params.searchTerm, "$options": "i" }}
    ]}
  ]},
    function (err, post) {
      if (err) return next(err);

      res.json(post);
  });
});

router.post('/', function(req, res, next) {
  req.body.created_by = req.user.local.email;
  Translation.create(req.body, function (err, post) {
    if (err) return next(err);

    if (!req.user.local.isAdmin)
    {
      mail('alina.cascaval@ferca.ro,corina@ferca.ro', 'Alerta adaugare termen', req.user.local.email + ' a adaugat un termen nou si necesita validare: http://ferca-glosar.eu-4.evennode.com/words#/validation');
    }

    //post.created_by = user.local.email;
    //Translation.update(post);
    res.json(post);
  });
});

router.put('/:id', function(req, res, next) {
  //TODO: if is admin, update directly otherwise add edit

 Translation.findById(req.params.id, function (err, post) {
   if (err) return next(err);

   req.body._id = mongoose.Types.ObjectId();
   req.body.validated = false;
   req.body.created_by = req.user.local.email;
   if (post.edits.length == 0) post.edits = [];

   post.edits.push(req.body);

   post.save(function (err) {
       if (err) return next(err);
     });

     if (!req.user.local.isAdmin)
     {
       mail('alina.cascaval@ferca.ro,corina@ferca.ro', 'Alerta editare termen', req.user.local.email + ' a editat un termen si necesita validare: http://ferca-glosar.eu-4.evennode.com/words#/validation');
     }

     res.json(post);

  });

});

router.put('/update_pending_edit/:editId', function(req, res, next) {
  var editId = req.params.editId;
  Translation.find({"edits._id": editId},
    function (err, translations) {
      if (err) return next(err);

      var translation = translations[0];
      translation.edits.forEach(function(edit, index){
        if (edit._id == editId){
          edit.English               = req.body.English;
          edit.Term_Example_English  = req.body.Term_Example_English;
          edit.Romanian              = req.body.Romanian;
          edit.Term_Example_Romanian = req.body.Term_Example_Romanian;
        }
      });

      translation.save(function (err) {
          if (err) return next(err);
        });

        res.json(translation);
      });

});

  /*  Translation.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);

    res.json(post);
  });*/

router.delete('/:id', function(req, res, next) {
  Translation.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);

    res.json(post);
  });
});

module.exports = router;
