// Load mongoose package
var mongoose = require('mongoose');
// Connect to MongoDB and create/use database called todoAppTest
mongoose.connect('mongodb://localhost/medict');
// Create a schema
var TranslationSchema = new mongoose.Schema({
  word: String,
  translation: Boolean,
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});
// Create a model based on the schema
var Translation = mongoose.model('Translation', TranslationSchema);

Translation.create({word: 'Create something with Mongoose', translation: 'this is one'}, function(err, trans){
  if(err) console.log(err);
  else console.log(trans);
});

Translation.find(function (err, translations) {
  if (err) return console.error(err);
  console.log(translations)
});

// callback function to avoid duplicating it all over
var callback = function (err, data) {
  if (err) { return console.error(err); }
  else { console.log(data); }
}
// Get ONLY completed tasks
Translation.find({word: /Mongoose$/ }, callback);

mongoose.disconnect();
