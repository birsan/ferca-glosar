var connection_string = 'mongodb://localhost/medict';
if(process.env.CONN){
  connection_string = process.env.CONN;
}
else if(process.env.ATLAS_MONGODB_DB_PASSWORD){
  connection_string = "mongodb+srv://" +
  process.env.ATLAS_MONGODB_DB_USERNAME + ":" +
  process.env.ATLAS_MONGODB_DB_PASSWORD + "@" +
  process.env.ATLAS_MONGODB_DB_HOST + ':' +
  process.env.ATLAS_MONGODB_DB_PORT + '/' +
  process.env.ATLAS_APP_NAME;
}

module.exports = {
    'url' : connection_string // looks like mongodb://<user>:<pass>@mongo.onmodulus.net:27017/Mikha4ot
};
